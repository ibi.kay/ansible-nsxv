This library contains playbooks for VMware NSX-V. Feel free to clone the whole repo, which contains the
required libraries. As a side note, the playbooks in this repo are using Hashicorp Vault for retrieving
the required secrets.



To do:

- [X]  Split out playbook to use roles

- [ ]  Fix routes in the playbook; abstract these out to the `all.yml` file
